import json

def read_file(name):
    with open(name, "r") as f:
        text = f.read()

    return text

def read_config():
    with open("config.json", "r") as f:
        data = json.load(f)

    return data
