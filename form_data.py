def parse_form_data(form_data):
    data_map = {y[0]:y[1] for y in [param.split("=") for param in form_data.split("&")]}

    parsed_data={}

    for key, value in data_map.items():
        key_parts = key.split("__")
        tmp = parsed_data

        for key_part in key_parts[:-1]:
            tmp = tmp.setdefault(key_part, {})

        try:
            number = int(value)
            value = number
        except ValueError:
            pass

        tmp[key_parts[-1]] = value

    return parsed_data

def generate_form(cfg, offset=4, level=2, path=[]):
    form_html = space_format("<ul>", offset)
    offset = offset + 1
    table_started = 0

    for key in sorted(cfg.keys()):
        value = cfg[key]

        if type(value) is dict:
            if table_started == 1:
                form_html += space_format("</table>", offset)
                offset = offset - 1
                table_started = 0

            form_html += "<h" + str(level) + ">" + key + "</h" + str(level) + ">"
            form_html += generate_form(value, offset, level+1, path + [key])
        else:
            if table_started == 0:
                offset = offset + 1
                form_html += space_format("<table>", offset)
                table_started = 1

            form_html += space_format("<tr>", offset)
            offset = offset + 1
            input_name = "__".join(path + [key])
            form_html += space_format("<td>" + key + "</td>", offset)
            form_html += space_format("<td><input name='" + input_name + "' value='" + str(value) + "'/></td>", offset)
            offset = offset - 1
            form_html += space_format("</tr>", offset)

    if table_started == 1:
        form_html += space_format("</table>", offset)
        table_started = 0
        offset = offset - 1

    offset = offset - 1

    form_html += space_format("</ul>", offset)

    return form_html


def space_format(string, offset):
    space = "  " * offset

    return "\n" + space + string
