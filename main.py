from machine import Pin, PWM
from time import sleep, time
import gc
from neopixel import NeoPixel

from mode_switcher import ModeSwitcher
from file_reader import read_config


hw_config = {
    "time": time(),
    "mode_switcher": ModeSwitcher(["setup","service"]),
    "np": NeoPixel(Pin(0, Pin.OUT), 3),
    "voltmeter_pwm": PWM(Pin(2, Pin.OUT), 25000)
}

config = read_config()

#Serial.begin(115200,SERIAL_8N1,SERIAL_TX_ONLY);

def irq_callback(p):
    global hw_config

    hw_config["np"] = NeoPixel(Pin(0, Pin.OUT), 1)
    
    if hw_config["time"] < time() - 2:
        hw_config["time"] = time()
        
        hw_config["np"][0] = (128, 0, 0)
        hw_config["np"].write()
            
        hw_config["mode_switcher"].set_next_mode()
        sleep(0.5)        

        machine.reset()


#pin.irq(trigger=Pin.IRQ_RISING, handler=irq_callback)

mode = hw_config["mode_switcher"].get_mode()
print(mode)


def hex_to_rgb(hex_color):
    cfg = config["Service"]

    hex_color = hex_color.lstrip('#')
    
    rgb_tuple = tuple(int(cfg["led_brightness"] * int(hex_color[i:i+2], 16)) for i in (0, 2, 4))
            
    return rgb_tuple


def payload_callback(data):
    ac_data = data["ac-input/"]
    bat_data = data["battery-state/"]
    inet_data = data["internet-state/"]

    hw_config["np"][0] = hex_to_rgb(ac_data.setdefault("led_color", "#FF00FF"))
    hw_config["np"][1] = hex_to_rgb(bat_data.setdefault("led_color", "#FF00FF"))
    hw_config["np"][2] = hex_to_rgb(inet_data.setdefault("led_color", "#FF00FF"))

    hw_config["np"].write()
    
    duty = int(820 / 100 * bat_data.setdefault("charge", 0))
    
    hw_config["voltmeter_pwm"].duty(duty)


if mode == "setup":
    from config import Config
    cfg = Config(hw_config)
    cfg.configure()
else:   
    from service import Service
    srv = Service(config, payload_callback)
    srv.serve()
