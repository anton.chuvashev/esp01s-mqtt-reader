"""
Original: https://github.com/troublegum/micropyserver
"""
import re
import socket
import sys
import io
import gc
from Url_encode import url_encode


class MicroPyServer(object):

    def __init__(self, host="0.0.0.0", port=80):
        """ Constructor """
        self._host = host
        self._port = port
        self._routes = []
        self._connect = None
        self._on_request_handler = None
        self._on_not_found_handler = None
        self._on_error_handler = None
        self._sock = None

    def start(self):
        """ Start server """
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._sock.bind((self._host, self._port))
        self._sock.listen(1)
        print("Server start")

        while True:
            if self._sock is None:
                break
            try:
                self._connect, address = self._sock.accept()
                header, body = self.get_request()

                if len(header) == 0:
                    self._connect.close()
                    continue

                if self._on_request_handler:
                    if not self._on_request_handler(header, address):
                        continue

                route = self.find_route(header)

                if route:
                    self.send(route["handler"](header, body))
                else:
                    self._route_not_found(header)
            except Exception as e:
                self._internal_error(e)
            finally:
                self._connect.close()

    def stop(self):
        self._connect.close()
        self._sock.close()
        self._sock = None
        print("Server stop")

    def add_route(self, path, handler, method="GET"):
        self._routes.append(
            {"path": path, "handler": handler, "method": method})

    def send(self, data):
        if self._connect is None:
            raise Exception("Can't send response, no connection instance")
        self._connect.sendall(data.encode())

    def find_route(self, request):
        lines = request.split("\r\n")
        method = re.search("^([A-Z]+)", lines[0]).group(1)
        path = re.search("^[A-Z]+\\s+(/[-a-zA-Z0-9_.]*)", lines[0]).group(1)

        for route in self._routes:
            if method != route["method"]:
                continue
            if path == route["path"]:
                return route
            else:
                match = re.search("^" + route["path"] + "$", path)
                if match:
                    print(method, path, route["path"])
                    return route

    def get_request(self, buffer_length=512):
        content_size = 0
        all_headers_read = 0
        request=''

        while not all_headers_read:
            request += self._connect.recv(512).decode("utf-8")

            for header in request.split("\r\n"):
                if header == "":
                    all_headers_read = 1
                elif header.startswith("Content-Length:"):
                    m = re.search(r'(\d+)', header)

                    if m:
                        content_size = int(m.group(0))

        headers, body = request.split("\r\n\r\n")

        del request
        gc.collect()

        while len(body) < content_size:
            body += self._connect.recv(512).decode("utf-8")

        url=url_encode()
        body = url.decode(body)

        return (str(headers), str(body))

    def on_request(self, handler):
        self._on_request_handler = handler

    def on_not_found(self, handler):
        self._on_not_found_handler = handler

    def on_error(self, handler):
        self._on_error_handler = handler

    def _route_not_found(self, request):
        if self._on_not_found_handler:
            self._on_not_found_handler(request)
        else:
            self.send("HTTP/1.0 404 Not Found\r\n")
            self.send("Content-Type: text/plain\r\n\r\n")
            self.send("Not found")

    def _internal_error(self, error):
        if self._on_error_handler:
            self._on_error_handler(error)
        else:
            if "print_exception" in dir(sys):
                output = io.StringIO()
                sys.print_exception(error, output)
                str_error = output.getvalue()
                output.close()
            else:
                str_error = str(error)
            self.send("HTTP/1.0 500 Internal Server Error\r\n")
            self.send("Content-Type: text/plain\r\n\r\n")
            self.send("Error: " + str_error)
            print(str_error)
