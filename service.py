from machine import Pin, reset
from time import sleep, time
import network
import socket
import json
from umqtt.robust2 import MQTTClient
from umqtt import simple2


__callback_data = {}


def mqtt_callback(topic, msg, retained, duplicate):
    msg = msg.decode("utf-8")  
    msg = "".join(msg.split("\n"))

    try:
        data = json.loads(msg)
        topic = topic.decode("utf-8") 

        print("MQTT: " + topic + "=" + json.dumps(data))

        __callback_data[topic] = data
        __callback_data["processing_required"] = 1
    except Exception as e:
        print(e)


class Service:
    def __init__(self, config, payload_callback):      
        self.config = config
        self.payload_callback = payload_callback
        self.last_keepalive = 0
        
    def connect_wifi(self):
        cfg = self.config["Service"]

        self.wlan = network.WLAN(network.STA_IF)
        self.wlan.active(True)
        self.wlan.connect(cfg["ssid"], str(cfg["password"]))

        checks_per_second = 4

        attempts_left = cfg["wifi_connect_wait_time"] * checks_per_second

        print("Waiting for WIFI connection")
        
        while not self.wlan.isconnected():
            if attempts_left == 0:
                print("WIFI connection failed, restart")
                self.restart()

            attempts_left -= 1

            print(".", end="")

            sleep(1/checks_per_second)

        print("\nWiFi connected")
        print(self.wlan.ifconfig())


    def connect_mqtt(self):
        cfg = self.config["MQTT"]

        client = MQTTClient(
            cfg["client_id"],
            cfg["server"],
            port=cfg["port"],
            user=cfg["user"],
            password=cfg["password"],
            keepalive=cfg["keepalive"],
        )

        client.set_callback(mqtt_callback)
        client.connect()
        
        print("Connected to MQTT")

        for topic in cfg["topics"]:
            __callback_data[topic] = {}
            client.subscribe(topic)

            print("Subscribed to " + topic)

        self.client = client

    def connect(self):
        try:
            self.connect_wifi()
            self.connect_mqtt()
        except Exception as e:
            print(e)
            print("Connect exception, restart")
            self.restart()

    def disconnect(self):
        self.wlan = None
        self.client = None
    
    def reconnect(self):
        print('WIFI reconnect')
        #self.disconnect()
        self.connect()

    def restart(self):
      reset()
      
    def serve(self):
        cfg = self.config["Service"]

        self.connect()

        while True:
            try:
                self.check_mqtt()
                self.process_callback_data()
            except Exception as e:
                print('Loop exception: ' + str(e) + '. Reconnect')
                self.reconnect()

            sleep(cfg["loop_time"])

    def process_callback_data(self):
        if __callback_data.setdefault("processing_required", 0):
            self.payload_callback(__callback_data)
            __callback_data["processing_required"] = 0

    def check_mqtt(self):
            self.client.check_msg()
            
            if self.last_keepalive < time() - self.config["MQTT"]["keepalive"]:
                self.client.ping()
                self.last_keepalive = time()
                   
            if self.client.is_conn_issue():
                print('MQTT connection error. Relogin')
                self.relogin_mqtt()                    
                
    def relogin_mqtt(self):
        reconnect_attempts=10
        
        while self.client.is_conn_issue():
            print('Reconnecting to MQTT')
            self.client.reconnect()
            reconnect_attempts-=1

            print(reconnect_attempts)

            sleep(1)
            
            if reconnect_attempts==0:
                reconnect_attempts=10
                self.reconnect()
        else:
            print('Resubscribing to MQTT')
            self.client.resubscribe()
